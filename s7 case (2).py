
import pandas as pd
import numpy as np

df={}
for k,i in enumerate(['Ангары','Авиакомпании','Типы ВС','Типы ТО','Потребность Авиакомпаний']):
    
    df[k+1]=pd.read_excel('C:/Users/user/Downloads/Оптимальная загрузка ангаров - входные данные.xlsx',
                     sheetname=i)

dr=pd.merge(df[5],df[3], on = 'Тип ВС')
dr=pd.merge(dr,df[2], on = 'Авиакомпания')


df[1].columns=['Ангар', 'w', 'l']
df[1]=df[1].set_index('Ангар').stack()
ind=df[1].index.tolist()
df[1].index=[i[0].split(' ')[0]+i[1] for i in ind]



df[4]=df[4].rename(columns={'ТО':'Формат ТО'})

dr=pd.merge(dr,df[4], on = ['Тип ВС',	'Формат ТО'])

dr=dr[list(set(dr.columns)-set(['Примечание_y','Примечание_x']))]

dr['грань']=((dr['Длина, м']/2)**2+(dr['Размах крыла, м']/2)**2)**0.5

v=['Минимальное количество форм ТО, \nкоторое необходимо выполнить по контракту, штук',
       'Потребность авиакомпании, штук']
dr['кол-во то']=dr[v].max(axis=1)
#dr=dr.drop(v,axis=1)

df[1]=pd.DataFrame(df[1]).transpose()
dr=pd.concat([dr,df[1]],axis=1)
dr[df[1].columns]=dr[df[1].columns].iloc[0].tolist()



dr=dr.rename(columns={'кол-во то':'num',
       
       'Штрафной коэффициент':'fee',
       'Окончание сезона технического обслуживания':'end',
       'Начало сезона технического обслуживания':'start',
        'Длительность, \nдней':'dur', 
       'SVO \n(стоимость 1 дня), руб':'SVO', 
       'VKO \n(стоимость 1 дня), руб':'VKO',
  
     'DME \n(стоимость 1 дня), руб':'DME',
     'грань':'side'})
    
mindate=dr[['start','end']].min().min()

dr['start']=dr['start']-mindate
dr['start']=dr['start'].map(lambda x: int(np.round(x.days/15)))

dr['end']=dr['end']-mindate
dr['end']=dr['end'].map(lambda x: int(np.round(x.days/15)))

dr['sh']=dr[['start','end']].apply(lambda x:[1 if i in list(np.arange(x[0]+1,x[1]+1)) else 0 for i in np.arange(9) ]  ,axis=1)

dr['dur']=dr.dur*dr.num

dr['dur']=dr['dur'].map(lambda x: int(np.round(x/15)))

dr['DMEs']=dr['DMEw']*dr['DMEl']
dr['SVOs']=dr['SVOw']*dr['SVOl']
dr['VKOs']=dr['VKOw']*dr['VKOl']

dr['ars']=dr['side']**2

dr['ars']=dr['ars'].astype(int)


dr[['SVO', 'VKO', 'DME']]=dr[['SVO', 'VKO', 'DME']].fillna(1000000)

dr[['SVO', 'VKO', 'DME']]=dr[['SVO', 'VKO', 'DME']]*15

dr[['SVO', 'VKO', 'DME']]=dr[['SVO', 'VKO', 'DME']].astype(int)


d=dr.copy()


dr=d.iloc[:50,:].copy()

#'dur'



from __future__ import print_function
from ortools.sat.python import cp_model


shift_requests = dr.sh.tolist()
per = len(shift_requests[0])




model = cp_model.CpModel()
var = {}
for n in range(per):
    for m in range(dr.shape[0]):
        var[('per',n,m)] = model.NewBoolVar('obs%iid%ip' % (n,m))
 
for n in ['SVO', 'VKO', 'DME']:
    for m in range(dr.shape[0]):
        var[('angar',n,m)]=model.NewBoolVar('angar'+n+str(m))

for m in range(dr.shape[0]):
    model.Add(sum(var[('per',n,m)] for n in range(per))==1)
    model.Add(sum(var[('per',n,m)]*shift_requests[m][n] for n in range(per))==1)
    
for m in range(dr.shape[0]):
    model.Add(sum(var[('angar',n,m)] for n in ['SVO', 'VKO', 'DME'])==1)
    
for n,lim in zip(['SVO', 'VKO', 'DME'],[ 24000, 18000, 10500 ]):
     model.Add(sum(var[('angar',n,m)]*dr.ars.iloc[m] for m in range(dr.shape[0]))<=lim)
    
model.Maximize(sum(var[('angar',n,m)]*dr[n].iloc[m] for m in range(dr.shape[0]) for n in ['SVO', 'VKO', 'DME']))

solver = cp_model.CpSolver()
solver.Solve(model)

t=[]
for n in range(per):
    for m in range(dr.shape[0]):
        if solver.Value(var[('per',n,m)]) == 1:
            print(n,m)
            t+=[{'period':n,'plane_id':m}]
        
l=[] 
for n in ['SVO', 'VKO', 'DME']:
    for m in range(dr.shape[0]):
        if solver.Value(var[('angar',n,m)]) == 1:
            print(n,m)
            l+=[{'airport':n,'plane_id':m}]
        
tt=pd.DataFrame(t).merge(pd.DataFrame(l),on='plane_id').sort_values('plane_id')


dr=pd.concat([tt,dr],axis=1)

import datetime # для работы с датами
from dateutil.relativedelta import relativedelta # для работы с датами
dr['period_start']=dr['period'].map(lambda x: +datetime.timedelta(days=x)*15+mindate)


dr=dr[['period_start','airport', 'Тип ВС','Формат ТО']]

print(dr)


dr.to_excel('C:/Users/user/Desktop/result.xlsx')